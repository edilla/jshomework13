let gama = false;
let gamma = localStorage.getItem('gamma');
let body = document.body.querySelector('.bod');
// debugger
if (gamma == 'true') {
    body.classList.add('secondgamma');
    gama = true;
    localStorage.setItem('gamma', 'false');
    gamma = localStorage.getItem('gamma');
}
let buttonR = document.createElement('button');
buttonR.innerHTML = 'another gamma';
buttonR.classList.add('gamma');
body.appendChild(buttonR);
buttonR.addEventListener('click', () => {
    if (gama == false) {
        body.classList.add('secondgamma');
        gama = true;
        localStorage.setItem('gamma', 'true');
        gamma = localStorage.getItem('gamma');
    } else {
        body.classList.remove('secondgamma');
        localStorage.setItem('gamma', 'false');
        gama = false;
        gamma = localStorage.getItem('gamma');
    }
});